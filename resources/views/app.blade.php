<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Difundir Naturaleza</title>

    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
<!--
    <link href="/assets/css/estilos.css" rel="stylesheet">
    <link href="/assets/css/custom.css" rel="stylesheet">
-->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    @yield('styles')

</head>
<body>
    <div class="page-header">
        <h1>Difundir Naturaleza</h1>
        <p>Proyecto Web de Acampe y Pesca</p>
    </div>
    @include('common.navbar')
<!--    @include('common.flash')-->

    <div class="container">
        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="/assets/js/scripts.js"></script>
    @yield('scripts')

</body>
</html>